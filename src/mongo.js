const MongoClient = require('mongodb').MongoClient;
const MongoServer = require('mongodb').Server;
const assert = require('assert');

mdb = {
	url: 'mongodb://localhost:27017/campeonato-brasileiro',
    logger: require('logger').createLogger('log/database.log'), 


	insertItem: async function(collection, item){

		client = await MongoClient.connect(mdb.url, { useNewUrlParser: true });
	  	var db = client.db();

		collection = db.collection(collection);
		result = await collection.insertOne(item);
		client.close();

		mdb.logger.info('Item inserted: ' + item.url);

		return result;

	},

	findItem: async function(collection, item, sort){

		client = await MongoClient.connect(mdb.url, { useNewUrlParser: true });
		var db = client.db();

		if(!sort){
			sort = {}
		}
		collection = db.collection(collection);
		result = await collection.find(item);
		result = result.toArray()
		client.close();
		
		return result;
	},

	deleteItem: async function(collection, item){

		client = await MongoClient.connect(mdb.url, { useNewUrlParser: true });
		var db = client.db();

		collection = db.collection(collection);
		result = await collection.deleteOne(item);
		client.close();
		
		return result;
	},

	updateItem: async function(collection, query, values){

		client = await MongoClient.connect(mdb.url, { useNewUrlParser: true });
		var db = client.db();

		newvalues = { $set: values }
		collection = await db.collection(collection);
		result = await collection.updateOne(query, newvalues);
		client.close();

		mdb.logger.info('Item updated');
		
		return result;
	},
	updateAddToSetItem: async function(collection, query, values){

		client = await MongoClient.connect(mdb.url, { useNewUrlParser: true });
		var db = client.db();

		newvalues = { $addToSet: values }
		collection = await db.collection(collection);
		result = await collection.updateOne(query, newvalues);
		client.close();

		mdb.logger.info('Item updated');
		
		return result;
	},

	insertPlayer: async function(player) {

		return await mdb.insertItem('player', player);
	},

	findPlayer: async function(player) {
	  
		return await mdb.findItem('player', player);
	},

	deletePlayer: async function(player) {
	  	return mdb.deleteItem('player', player);
	},

	updatePlayer: async function(query, values) {
	  	return await mdb.updateItem('player', query, values);
	},

	addRatingPlayer: async function(query, values) {
	  	return await mdb.updateAddToSetItem('player', query, values);
	},

	insertTeam: async function(team) {
	
		return await mdb.insertItem('team', team);
	},

	updateTeam: async function(query, values) {
	  	return await mdb.updateItem('team', query, values);
	},

	findTeam: async function(team) {
	  
		return await mdb.findItem('team', team);
	},

	deleteTeam: async function(team) {
	  	return await mdb.deleteItem('team', team);
	},

	insertMatch: async function(match) {
	
		return await mdb.insertItem('match', match);
	},

	findMatch: async function(match) {
	  
		return await mdb.findItem('match', match);
	},

	deleteMatch: async function(match) {
	  	return await mdb.deleteItem('match', match);
	},

	insertChampionship: async function(championship) {
	
		return await mdb.insertItem('championship', championship);
	},

	findChampionship: async function(championship, sort) {
	  
		return await mdb.findItem('championship', championship, sort);
	},

	deleteChampionship: async function(championship) {
	  	return await mdb.deleteItem('championship', championship);
	},

	updateChampionship: async function(query, values) {
	  	return await mdb.updateItem('championship', query, values);
	}
}

module.exports = mdb;