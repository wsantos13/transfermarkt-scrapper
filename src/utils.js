const https = require('https');
utils = {

    logger: require('logger').createLogger('log/requests.log'), 

    sleep: function(ms){
        return new Promise(resolve=>{
            setTimeout(resolve,ms)
        });
    },

    getRandomArbitrary: function (min, max) {
      return Math.random() * (max - min) + min;
    },

    getPage: async function(options){

        //var espera = utils.getRandomArbitrary(100, 200);
        //await utils.sleep(espera);

        return new Promise( async function(resolve, reject) {

            const playerLink = options.hostname + options.path;
            await utils.sleep(utils.getRandomArbitrary(100, 200));
            https.get(options, (resp) => {

                let data = '';

                resp.on('data', (chunk) => {
                    data += chunk;
                });

                resp.on('end', () => {
                    page = data;
                    utils.logger.info('page received options: ' + options.hostname + options.path);
                    return resolve(page);
                });

            }).on("error", async (err) => {
                utils.logger.error(err.code + ' options: ' + options.hostname + options.path);
                await utils.sleep(utils.getRandomArbitrary(500, 2000));
                return await utils.getPage(options);
            }).on('timeout', async () => {
                utils.logger.error('timeout options: ' + options.hostname + options.path);
                await utils.sleep(utils.getRandomArbitrary(500, 2000));
                return await utils.getPage(options);
            });
        });
	},

    buildOptions: function(link){

    return  {
                hostname: 'www.transfermarkt.com',
                path: link,
                headers: {
                    'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0'
                },
                timeout: 3000
            };
    }
}

module.exports = utils