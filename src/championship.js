const matchScrapper = require('./match.js')
const https = require('https');
const htmlParser = require('node-html-parser');
const database = require('./mongo.js');
const utils = require('./utils.js');

championship_scrapper = {

	logger: require('logger').createLogger('log/run.log'),

	scrapper: async function(options){


		var page = await utils.getPage(options);
		var champshipLink = options.hostname + options.path;

		var root = htmlParser.parse(page);

		name = root.querySelector(".spielername-profil").rawText;
		year = parseInt(options['path'].substring(options['path'].length-4, options['path'].length)) + 1;
		selector = root.querySelectorAll("html div.large-6.columns div.box"); 

		matchDaysLinks = []

		selector.forEach( boxSelector => {

			linksSelector = boxSelector.querySelectorAll("td a.ergebnis-link");

			matchDayLinks = []

			linksSelector.forEach(linkSelector => {
				var attr = linkSelector.rawAttrs.split("href=");
				var href = attr[1].replace(/\"/g, "");
				matchDayLinks.push(href);
			});
			matchDaysLinks.push(matchDayLinks);
		});

		matchDays = {};

		
		for( [index, matchDayLinks] of matchDaysLinks.entries()) {

			matchDay = []

			for( link of matchDayLinks){

				dbMatches = await database.findMatch({ 'url': options.hostname + link });
				
				try {
					var match;
					if( dbMatches.length > 0){
						match = dbMatches[0];
					} else{
						match = await matchScrapper.scrapper(utils.buildOptions(link));
						result = await database.insertMatch(match);
						match = result.ops[0];
					}

					matchDay.push(match);
				} catch ( exception ){
					championship_scrapper.logger.error(exception.stack);
					championship_scrapper.logger.error(options);
					championship_scrapper.logger.error(link);
				}
				
			}

			matchDays[index] = matchDay;
		}


		return 	{ 
					'name': name,
					'year': year,
					'url': champshipLink,
					'matchDays': matchDays 
				};
	}
}

module.exports = championship_scrapper