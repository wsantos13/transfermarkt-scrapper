var crypto = require('crypto');
const htmlParser = require('node-html-parser');
const utils = require('./utils.js');

player_scrapper = {

	scrapper: async function(options){

        var page = await utils.getPage(options);
        var playerLink = options.hostname + options.path;

        const root = htmlParser.parse(page);
        var infoTable = root.querySelectorAll("div.spielerdaten table.auflistung tr");

        var name = '';
        var birthday = '';
        var position = '';

        for(info of infoTable){
            playerInfo = info.querySelector("th");

            if(playerInfo.rawText.trim().match(/.*Full Name.*/)){
                name = info.querySelector("td").rawText.trim();
            }

            if(playerInfo.rawText.trim() == "Date of Birth:"){
                birthday = info.querySelector("td").rawText.trim();
            }

            if(playerInfo.rawText.trim() == "Position:"){
                position = info.querySelector("td").rawText.trim();
            }
        }

        if(name == ''){
            name = root.querySelector("div.dataTop div.dataName h1 b").innerText;
        }

        birthday = new Date(infoTable[1].rawText.trim());

        var md5sum = crypto.createHash('md5');
        md5sum.update(name+birthday);
        var hash = md5sum.digest('hex');
        return {
                "name": name,
                "birthday": birthday,
                "position": position,
                "url": playerLink,
                "rating-history": [],
                "hash": hash
            };
	}
}

module.exports = player_scrapper