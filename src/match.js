const teamScrapper = require('./team.js');
const playerScrapper = require('./player.js');
var crypto = require('crypto');

const https = require('https');
const htmlParser = require('node-html-parser');

const utils = require('./utils.js');

const database = require('./mongo.js');

match_scrapper = {

	logger: require('logger').createLogger('log/run.log'),

	scrapper: async function(options){
		const matchLink = options.hostname + options.path;		
		var page = await utils.getPage(options);

		const root = htmlParser.parse(page);
		date = root.querySelectorAll(".sb-datum.hide-for-small a"); 

		matchDate = new Date(date[1].innerHTML);

		var homeTeamLink = root.querySelector(".sb-team.sb-heim.hide-for-small .sb-vereinslink");
		homeTeamLink = homeTeamLink.rawAttrs.split("href=");
		homeTeamLink = homeTeamLink[1].replace(/\"/g, "");
		
		var awayTeamLink = root.querySelector(".sb-team.sb-gast.hide-for-small .sb-vereinslink");
		awayTeamLink= awayTeamLink.rawAttrs.split("href=");
		awayTeamLink = awayTeamLink[1].replace(/\"/g, "");
		var score = root.querySelectorAll(".sb-ergebnis .sb-endstand");
		score = score[0].childNodes[0].rawText.trim();
		scoreSplit = score.split(':');
		var homeScore = parseInt(scoreSplit[0])
		var awayScore = parseInt(scoreSplit[1])

		var playersTables = root.querySelectorAll("div.row div.large-12.columns div.box div.large-6.columns");
		links = playersTables[0].querySelectorAll('.spielprofil_tooltip');
		
		var homeLinks = [];


		for(link of links){
			
			
			if(link != null && !link.rawAttrs.match(/.*aufstellung-rueckennummer-box.*/)){
				
				if(!link.rawAttrs.match(/.*href=.*/)){

					linkkkk = link.querySelector('a')
					playerLink= linkkkk.rawAttrs.split("href=");
				}else {
					playerLink= link.rawAttrs.split("href=");
				}	

				playerLink = playerLink[1].replace(/\"/g, "");
				
				homeLinks.push(playerLink);

				if(homeLinks.length >= 11){
					break;
				}
			}	
		}

		links = playersTables[1].querySelectorAll('.spielprofil_tooltip');
		var awayLinks = [];

		for(link of links){


			if(!link.rawAttrs.match(/.*aufstellung-rueckennummer-box.*/)){

				if(!link.rawAttrs.match(/.*href=.*/)){
					linkkkk = link.querySelector('a')
					playerLink= linkkkk.rawAttrs.split("href=");
				}else {
					playerLink= link.rawAttrs.split("href=");
				}
				
				playerLink = playerLink[1].replace(/\"/g, "");
			
				awayLinks.push(playerLink);

				if(awayLinks 	.length >= 11){
					break;
				}
			}

		}

		homeTeamDb = await database.findTeam({ 'url': options.hostname + homeTeamLink.split('saison_id')[0]});

		if( homeTeamDb.length > 0){
			homeTeam = homeTeamDb[0];
		} else {
			homeTeam = await teamScrapper.scrapper(utils.buildOptions(homeTeamLink));
			result = await database.insertTeam(homeTeam);
			homeTeam = result.ops[0];
		}

		awayTeamDb = await database.findTeam({ 'url': options.hostname + awayTeamLink.split('saison_id')[0]});

		if( awayTeamDb.length > 0){
			awayTeam = awayTeamDb[0];
		} else {
			awayTeam = await teamScrapper.scrapper(utils.buildOptions(awayTeamLink));
			result = await database.insertTeam(awayTeam);
			awayTeam = result.ops[0];
		}
		
		homePlayers = []

		for(homePlayerLink of homeLinks){

			homePlayerDb = await database.findPlayer({ 'url': options.hostname + homePlayerLink } );

			try{
				if(homePlayerDb.length > 0 ){
					homePlayer = homePlayerDb[0];
				} else {
					homePlayer = await playerScrapper.scrapper(utils.buildOptions(homePlayerLink));
					result = await database.insertPlayer(homePlayer);
					homePlayer = result.ops[0];
				} 
			}catch (exception){

				match_scrapper.logger.error(exception.stack);
				match_scrapper.logger.error(options);
				match_scrapper.logger.error(utils.buildOptions(homePlayerLink));
			}
			

			homePlayers.push(homePlayer);
		};

		awayPlayers = []

		for(awayPlayerLink of awayLinks){
			
			awayPlayerDb = await database.findPlayer({ 'url': options.hostname + awayPlayerLink } );

			try{
				if(awayPlayerDb.length > 0 ){
					awayPlayer = awayPlayerDb[0];
				} else {
					awayPlayer = await playerScrapper.scrapper(utils.buildOptions(awayPlayerLink));
					result = await database.insertPlayer(awayPlayer);
					homePlayer = result.ops[0];
				}

			}catch (exception){
				match_scrapper.logger.error(exception.stack);
				match_scrapper.logger.error(options);
				match_scrapper.logger.error(utils.buildOptions(awayPlayerLink));
			}

			awayPlayers.push(awayPlayer);
		};

		var md5sum = crypto.createHash('md5');
		md5sum.update(homeTeam+awayTeam+date);
		var hash = md5sum.digest('hex');

		return {
			"url": matchLink,
            "homeTeam": homeTeam,
            "awayTeam": awayTeam,
            "homeScore": homeScore,
            "awayScore": awayScore,
            "homePlayers": homePlayers,
            "awayPlayers": awayPlayers,
            "matchDate": matchDate,
            "hash": hash
        };
	} 

}

module.exports = match_scrapper