const puppeteer = require('puppeteer');
const path = require('path')
const playerScrapper = require('./player.js');
const championshipScrapper = require('./championship.js');
const matchScrapper = require('./match.js');
const teamScrapper = require('./team.js');
const utils = require('./utils.js');

const database = require('./mongo.js');

const https = require('https');
const htmlParser = require('node-html-parser');

const SERIE_A = "https://www.transfermarkt.com/campeonato-brasileiro-serie-a/gesamtspielplan/wettbewerb/BRA1?saison_id=2018"
const SERIE_B = "https://www.transfermarkt.com/campeonato-brasileiro-serie-b/gesamtspielplan/wettbewerb/BRA2?saison_id=2018"

transfermarkt_scrapper = {


	_startBrowser: async function(season) {
    
        const browserFetcher = puppeteer.createBrowserFetcher({
            path: process.cwd()
        });

        //TODO argv proxy server
        //var pptrArgv = [];
        //if (argv.proxyURI) {
        //    pptrArgv.push( '--proxy-server=' + argv.proxyURI );
       	// }

        //TODO parametrizar configuracoes do puppeteer
        const browser = await puppeteer.launch({
            executablePath: "/usr/bin/google-chrome",
            headless: true,
            userDataDir: path.join(process.cwd(), "resources/chrome-sessions/" + season),
            devtools: false,
            timeout: 5000000
        //    args: pptrArgv
        });

        return browser;
	},

	getYearPage: async function(page, year){

		selector = await page.$$eval("[name=\"saison_id\"]", (sel, year) => {sel[0].value = year;}, year-1);
		button = await page.$eval("[value=\"Show\"]", async but => await but.click());
		return page;
	},

	scrapper: async function(){
		seasons = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
		championships =[]
		for(season of seasons){
			champSerieA = await championshipScrapper.scrapper(utils.buildOptions('/campeonato-brasileiro-serie-a/gesamtspielplan/wettbewerb/BRA1?saison_id=' + (season-1).toString()));
			dbChamp = await database.findChampionship({ 'url': champSerieA.url });

			if( dbChamp.length > 0){
				database.updateChampionship({ 'url': champSerieA.url }, champSerieA);
			} else{
				result = await database.insertChampionship(champSerieA);
				champSerieA = result.ops[0];
			}
			//console.log(champSerieA);

			champSerieB = await championshipScrapper.scrapper(utils.buildOptions('/campeonato-brasileiro-serie-b/gesamtspielplan/wettbewerb/BRA2?saison_id=' + (season-1).toString()));

			dbChamp = await database.findChampionship({ 'url': champSerieB.url });

			if( dbChamp.length > 0){
				database.updateChampionship({ 'url': champSerieB.url }, champSerieB);
			} else{
				result = await database.insertChampionship(champSerieB);
				champSerieB = result.ops[0];
			}
			//console.log(champSerieB);

		};		
	}
}

transfermarkt_scrapper.scrapper()




