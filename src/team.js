var crypto = require('crypto');

const https = require('https');
const htmlParser = require('node-html-parser');

const utils = require('./utils.js');

team_scrapper = {

	scrapper: async function(options){

        var link = options.hostname + options.path;

        //https://www.transfermarkt.com/sport-club-do-recife/startseite/verein/8718/saison_id/2008
        link = link.split('saison_id')[0]
        
        var page = await utils.getPage(options);

        const root = htmlParser.parse(page);
        teamName = root.querySelector(".dataName h1");
        teamName = teamName.rawText.trim();

        var md5sum = crypto.createHash('md5');
        md5sum.update(teamName);
        var hash = md5sum.digest('hex');

        return {
            "name": teamName,
            "url": link,
            "rating-history": [],
            "hash": hash
            };
    }
}

module.exports = team_scrapper